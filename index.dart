// Array called List
spreadOperator() {
  var list = [1,2,3];
  var list2 = [0, ...list];
  print(list2.length == 4);
}

nullSpreadOperator() {
  var list;
  var list2 = [0, ...?list];
  print(list2.length == 1);
}

collectionsFor() {
  var listOfInts = [1,2,3];
  var listOfStrings = [
    '#0',
    for (var i in listOfInts) '#$i'
  ];
  print(listOfStrings);
}

// Objects called Sets
halogens() {
  var halogens = {'fluorine', 'chlorine', 'bromine', 'iodine', 'astatine'};
  // print(halogens);

  // Set<String> names = {}; // This works, too.
  // var names = {}; // Creates a map, not a set.
  var names = <String>{};
  names.add('new');
  names.addAll(halogens);

  print(names);
}

// Maps is an object that associates keys and values.
sampleMaps() {
  // var gifts = {
  // // Key:    Value
  // 'first': 'partridge',
  // 'second': 'turtledoves',
  // 'fifth': 'golden rings'
  // };

  // var nobleGases = {
  //   2: 'helium',
  //   10: 'neon',
  //   18: 'argon',
  // };
  var gifts = Map();
  // Add key-value pair
  gifts['first'] = 'partidge';
  gifts['second'] = 'turtledoves';
  gifts['fifth'] = 'golden rings';

  // print(gifts);
  // print(gifts['first'] == 'partridge');

  // var nobleGases = Map();
  // nobleGases[2] = 'helium';
  // nobleGases[10] = 'neon';
  // nobleGases[18] = 'argon';
  final constantMap = const {
    2: 'helium',
    10: 'neon',
    18: 'argon',
  };
  // constantMap[2] = 'Helium'; // Not allowed to add key-value pair causes an error the map is constant.
  print(constantMap);
}

bool isNoble(int atomicNumber) {
  var nobleGases = {
    2: 'helium',
    10: 'neon',
    18: 'argon',
  };
  return (nobleGases[atomicNumber] != null);
}

// Positional parameters [] on function
// Wrapping a set of function parameters in [] marks them as optional positional parameters:
String say(String from, String msg, [String device = 'carrier pigeon', String mood]) {
  var result = '$from says $msg';
  if (device != null) {
    result = '$result with a $device';
  }
  if (mood != null) {
    result = '$result (in a $mood mood)';
  }
  return result;
}

class Spacecraft {
  String name;
  DateTime launchDate;

  // Constructor
  Spacecraft(this.name, this.launchDate) {

  }

  Spacecraft.unlaunched(String name) : this(name, null);

  int get launchYear => launchDate?.year;

  void describe() {
    print('Spacecraft: $name');
    if (launchDate != null) {
      int years = DateTime.now().difference(launchDate).inDays ~/ 365;
      print('Launched: $launchYear ($years years ago)');
    } else {
      print('Unlaunched');
    }
  }
}

// Inheritance or extending classes
class Orbiter extends Spacecraft {
  num altitude;
  Orbiter(String name, DateTime launchDate, this.altitude) : super(name, launchDate);
}

main() {
  // print(say('Bob', 'Howdy', 'dog', 'mad'));
  var voyager = Spacecraft('Voyager I', DateTime(1977, 9, 5));
  voyager.describe();
  var voyager3 = Spacecraft.unlaunched('Voyager III');
  voyager3.describe();

  var orb = Orbiter('Mezahiv', DateTime(2005, 2, 8), 12);
  orb.describe();
}
  